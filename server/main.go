package main

import (
	"encoding/json"
	"github.com/codegangsta/cli"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
)

type ScoreBoard struct {
	sync.RWMutex
	Score map[string]int
}

var max = 100

var scoreBoard = &ScoreBoard{Score: map[string]int{
	"1": 1,
	"2": 1,
}}

func add(w http.ResponseWriter, r *http.Request) {
	scoreBoard.Lock()
	defer scoreBoard.Unlock()

	r.ParseForm()
	id := r.Form.Get("id")
	score, _ := strconv.ParseInt(r.Form.Get("score"), 10, 32)
	scoreBoard.Score[id] += int(score)

	if scoreBoard.Score[id] >= max {
		for i, _ := range scoreBoard.Score {
			scoreBoard.Score[i] /= 2
			if scoreBoard.Score[i] <= 0 {
				scoreBoard.Score[i] = 1
			}
		}
	}
	log.Println("add", scoreBoard.Score)
}

func get(w http.ResponseWriter, r *http.Request) {
	scoreBoard.RLock()
	defer scoreBoard.RUnlock()

	log.Println("get", scoreBoard.Score)
	encoder := json.NewEncoder(w)
	encoder.Encode(scoreBoard.Score)
}

func start(c *cli.Context) {
	addr := c.String("addr")
	http.Handle("/", http.FileServer(http.Dir("static")))
	http.HandleFunc("/add", add)
	http.HandleFunc("/get", get)
	http.ListenAndServe(addr, nil)
}

func main() {
	app := cli.NewApp()

	app.Action = start
	app.Flags = []cli.Flag{
		cli.StringFlag{"addr", ":8080", "address of server"},
	}

	app.Run(os.Args)
}
