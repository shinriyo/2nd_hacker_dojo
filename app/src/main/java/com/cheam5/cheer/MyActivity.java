package com.cheam5.cheer;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.hardware.SensorEventListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MyActivity extends Activity implements SensorEventListener {
    private final int WC = ViewGroup.LayoutParams.WRAP_CONTENT;
    private SensorManager manager;
    // フィルタ対象
    private static final float FILTERING_VALUE = 0.1f;
    // ローパスフィルタされた値
    private float lowX, lowY, lowZ;
    // SensorManagerのインスタンス
    private int id = 0;

    private long time = 0L;

    private View.OnClickListener japanClicked = new View.OnClickListener() {
        public void onClick(View v) {
            id = 1;
            Toast.makeText(MyActivity.this, "日本", Toast.LENGTH_LONG).show();
            Log.v("Button","Japan onClick");
        }
    };

    private View.OnClickListener brazilClicked = new View.OnClickListener() {
        public void onClick(View v) {
            id = 2;
            Toast.makeText(MyActivity.this, "ブラジル", Toast.LENGTH_LONG).show();
            Log.v("Button","Brazil onClick");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        manager = (SensorManager)getSystemService(SENSOR_SERVICE);

        // Japan
        //buttonを取得
        Button japanBtn = (Button)findViewById(R.id.japan_btn);
        japanBtn.setOnClickListener(japanClicked);
        // Brazil
        Button brzilBtn = (Button)findViewById(R.id.brazil_btn);
        brzilBtn.setOnClickListener(brazilClicked);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        // Listenerの登録解除
        manager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        // Listenerの登録
        List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if(sensors.size() > 0) {
            Sensor s = sensors.get(0);
            manager.registerListener(this, s, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[SensorManager.DATA_X];
        float y = event.values[SensorManager.DATA_Y];
        float z = event.values[SensorManager.DATA_Z];
        lowX = x * FILTERING_VALUE + lowX * (1.0f - FILTERING_VALUE);
        lowY = y * FILTERING_VALUE + lowY * (1.0f - FILTERING_VALUE);
        lowY = z * FILTERING_VALUE + lowZ * (1.0f - FILTERING_VALUE);
        // ハイパスフィルタ処理
        float highX = x - lowX;
        float highY = y - lowY;
        float highZ = z - lowY;

        Log.d("hoge", "" + highX);
        Log.d("hoge", "" + highY);
        Log.d("hoge", "" + highZ);

        final int score = Math.abs((int)highX) + Math.abs((int)highY) + Math.abs((int)highZ);

        long currentTime = System.currentTimeMillis();
        long minuits = 1000;

        if (currentTime - time <= minuits)
        {
            return;
        } else {
            time = currentTime;
        }

        try {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        getToHttp(score);
                    }catch (Exception e) {
                        Log.e("hoge", e.toString());
                    }
                }
            });
            t.start();
        } catch (Exception e) {
            Log.e("hoge", e.toString());
        }

        /*
        // httpリクエストを入れる変数(面倒なので何も入れない)
        Uri.Builder builder = new Uri.Builder();

        AsyncHttpRequest task = new AsyncHttpRequest(this);
        task.execute(builder);
        int score = Math.abs((int)highX) + Math.abs((int)highY) + Math.abs((int)highZ);
        task.SetScore (score);*/
    }

    public void getToHttp (int score) throws IOException {
        //int id = 2;
        /*
        int editTextId = R.id.editText;
        EditText editText = (EditText)findViewById(editTextId);
        editText.selectAll(); // おまじない

        id = 0;

        try {
            id = Integer.parseInt(editText.getText().toString());
        } catch (Exception e){
            Log.e("hoge", e.toString());
        }
        */


        /*
        HttpURLConnection con = null;
        String urlStr = "http://192.168.248.190:9090/add";
        //URL url = new URL("http://localhost:9090/add?id="+id+"&score="+score);
        String finalUrlStr = urlStr + "?id="+id+"&score="+score;
        Log.e("hoge", finalUrlStr);
        URL url = new URL(finalUrlStr);
        con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("GET");
        con.setInstanceFollowRedirects(false);
        con.connect();*/

        String urlStr = "http://192.168.248.190:9090/add";
        String finalUrlStr = urlStr + "?id="+id+"&score="+score;

        try {
            URL url = new URL(finalUrlStr);
            HttpURLConnection urlcon;
            try {
                urlcon = (HttpURLConnection)url.openConnection();
                urlcon.setRequestMethod("GET");

                urlcon.connect();

                BufferedReader reader = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));
                while (true){
                    String line = reader.readLine();
                    if ( line == null ){
                        break;
                    }
                }
                reader.close();
                urlcon.disconnect();
            } catch (IOException ex) {
                Log.d("hoge", ex.toString());
            }
        } catch (MalformedURLException ex) {
            Log.d("hoge", ex.toString());
        }
    }
}